def isMultiplo (n1, n2):
    if n1 % n2 == 0:
        return True
    return False

num_1 = int(input("Digite um número: "))
num_2 = int(input("\nDigite outro número: "))

if isMultiplo(num_1, num_2):
    print(f"\nO número {num_1} é multiplo de {num_2}\n")
else:
    print(f"\nO número {num_1} não é multiplo de {num_2}\n")